all: proto tracker tracker-controller trackerd

OS_NAME := $(shell uname -s | tr A-Z a-z)
SED_COMMAND:=sed

ifeq ($(OS_NAME),darwin)
	SED_COMMAND=gsed
endif

proto:
	docker run --user $(shell id -u):$(shell id -g) -v $(shell pwd):/workspace --rm grpckit/omniproto:1.30_0

tracker:
	make -C cmd tracker

tracker-controller:
	make -C cmd tracker-controller

trackerd:
	make -C cmd trackerd

